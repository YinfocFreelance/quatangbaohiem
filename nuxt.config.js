import webpack from 'webpack'

export default {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: 'Quà Tặng Bảo Hiểm',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'title', content: 'Quà tặng bảo hiểm' },
      { hid: 'description', name: 'description', content: 'Quà Tặng Bảo Hiểm - Ứng dụng hỗ trợ làm việc cho đội ngũ tư vấn tài chính, tư vấn bảo hiểm tại Việt Nam' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/images/favicon.png' },
      { rel: 'shortcut icon', href: '/images/favicon.png' },
      { rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.10/css/AdminLTE.min.css' },
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js', },
      { src: 'https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.10/js/adminlte.min.js', },
      { src: '/js/fb-sdk.js' },
      { src: 'https://connect.facebook.net/vi_VN/sdk.js', defer: true , async: true, crossorigin: "anonymous" },
      { src: 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js', defer: true , async: true },
      { src: 'https://sdk.accountkit.com/vi_VN/sdk.js',},
      { src: '/js/google-tag.js' },
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#31a00d',
    height: '3px'
  },

  // server: {
  //   port: 8000, // default: 3000
  //   host: '0.0.0.0', // default: localhost
  // },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/admin.css',
    '~/assets/css/font-awesome.css',
    '~/assets/css/icomoon.css',
    '~/assets/scss/index.scss',
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/axios',
    '~/plugins/common',
    { src: '~/plugins/vee-validate.js', ssr: false },
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/toast',
    '@nuxtjs/proxy'
  ],

  axios: {
    //prefix: '/api',
    baseURL: 'https://quatangbaohiem.com/api'
    //proxy: true
  },

  proxy: {
    '/api': {
      target: 'https://quatangbaohiem.com/api',
      pathRewrite: {
        '^/api' : ''
      }
    }
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    },

    plugins: [
      new webpack.NormalModuleReplacementPlugin(
        /element-ui[\/\\]lib[\/\\]locale[\/\\]lang[\/\\]zh-CN/,
        'element-ui/lib/locale/lang/en',
      ),
    ],
  }
}
