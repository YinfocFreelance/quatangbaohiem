import Vue from 'vue'
import VeeValidate, {Validator} from 'vee-validate'

const dictionary = {
  vi: {
    // messages: {
    //   phone: field => `${field} nhập vào không hợp lệ`,
    // },
    attributes: {
      first_name: 'Tên',
      last_name: 'Họ và đệm',
      password: 'Mật khẩu',
      password_confirmation: 'Mật khẩu nhắc lại',
      phone: 'Số điện thoại',
      customer_phone: 'Số điện thoại',
      email: 'Email',
      cancellation_reason: 'Lý do hủy đặt phòng'
    }
  },

  en: {
    attributes: {
      first_name: 'First name',
      last_name: 'Last name',
      password: 'Password',
      password_confirmation: 'Password confirmation',
      phone: 'Phone',
      customer_phone: 'Phone',
      email: 'Email',
      cancellation_reason: 'Cancellation reason'
    }
  },
}

export default ({store}) => {
  Vue.use(VeeValidate, {
    locale: 'vi',
    events: 'blur',
    errorBagName: 'veeErrors',
    fieldsBagName: 'veeFields',
    inject: false,
  })
}
