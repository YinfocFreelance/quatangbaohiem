import Cookie from 'js-cookie'
import {objectToStringQuery, stringifyQuery} from '~/utils/route'
import Vue from 'vue'

export default ({ $axios, $toast }) => {
  const callUserApiGet = async (path, payload, pageSection) => {
    if(pageSection) {
      payload.StartIndex = pageSection[0]
      payload.MaxResult = pageSection[1]
    }
    payload.UserId = Cookie.getJSON('user_info').userId
    payload.Token = Cookie.getJSON('user_info').token
    const data = await $axios.$get( path + stringifyQuery(payload))
    //if( data.Reason ) $toast.error(data.Reason, {duration: 5000})
    if( data.ErrorCode && data.ErrorCode === '001' ) {
      Cookie.remove('user_info')
      localStorage.removeItem('user_info')
      localStorage.removeItem('common')
      location.href = '/'
    }
    return data
  }

  const callApiGet = async (path, payload, pageSection) => {
    if(pageSection) {
      payload.StartIndex = pageSection[0]
      payload.MaxResult = pageSection[1]
    }
    const data = await $axios.$get( path + objectToStringQuery(payload))
    //if( data.Reason ) $toast.error(data.Reason, {duration: 5000})
    if( data.ErrorCode && data.ErrorCode === '001' ) {
      Cookie.remove('user_info')
      localStorage.removeItem('user_info')
      localStorage.removeItem('common')
      localStorage.removeItem('customers')
      location.href = '/'
    }
    return data
  }

  const removeDataSignIn = () =>  {
    localStorage.removeItem('user_info')
    localStorage.removeItem('common')
    localStorage.removeItem('customers')
    Cookie.remove('user_info')
  }

  const setDataSignIn = ( user, common ) =>  {
    localStorage.setItem('customers', JSON.stringify(user.Customers))
    localStorage.setItem('user_info', JSON.stringify(user))
    localStorage.setItem('common', JSON.stringify(common))
    Cookie.set('user_info', {
      userId : user.UserId,
      token : user.Token
    })
  }

  Vue.mixin({
    methods: {
      $_callUserApiGet: callUserApiGet,
      $_callApiGet: callApiGet,
      $_removeDataSignIn: removeDataSignIn,
      $_setDataSignIn: setDataSignIn,
    }
  })
}
