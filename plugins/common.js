import Vue from 'vue'
export default () => {
  const convertUrlHttps = (url) => {
    return url.replace('http://', 'https://')
  }

  Vue.mixin({
    methods: {
      $_convertUrlHttps: convertUrlHttps
    }
  })
}
