import { Select, Option, Radio, Checkbox } from 'element-ui'
import Vue from 'vue'
Vue.component('el-select',Select)
Vue.component('el-option',Option)
Vue.component('el-radio',Radio)
Vue.component('el-checkbox',Checkbox)
