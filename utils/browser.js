export const scrollToEl = selector => {
  let browser = navigator.userAgent

  if (browser.indexOf('MSIE') >= 0) browser = 'MSIE'
  else if (browser.indexOf('Firefox') >= 0) browser = 'Firefox'
  else if (browser.indexOf('Chrome') >= 0) browser = 'Chrome'
  else if (browser.indexOf('Safari') >= 0) browser = 'Safari'

  const firstErrorEl = document.querySelector(selector)
  if (firstErrorEl) {
    let scroll = firstErrorEl.offsetTop - window.innerHeight / 2

    if (browser !== 'Safari')
      window.scrollTo({ top: scroll, behavior: 'smooth' })
    else window.scrollTo(0, scroll)
  }
}
