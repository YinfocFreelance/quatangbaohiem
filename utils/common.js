export const parseCookies = (cookie) => {
  let cookieData = (typeof cookie === 'string' ? cookie : '').trim()

  return (cookieData ? cookieData.split(';') : []).reduce(function(cookies, cookieString) {
    cookieData = cookieString.split('=')
    cookies[cookieData[0].trim()] = cookieData.length > 1 ? cookieData[1].trim() : ''
    return cookies
  }, {})
}

export const getIDYoutube = (link) => {
  if(link) return link.split("v=").pop()
  else return ''
}
