export const defaultGlideOpts = {
  gap: 16, // px
  keyboard: true,
  rewind: false,
  bound: true,
  peek: {
    before: 0,
    after: 0
  },
}
