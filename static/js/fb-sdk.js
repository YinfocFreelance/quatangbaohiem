window.fbAsyncInit = function() {
  FB.init({
    appId            : '461593864589804',
    autoLogAppEvents : true,
    xfbml            : true,
    version          : 'v3.3',
  });
};
window.AccountKit_OnInteractive = function(){
  AccountKit.init(
    {
      appId:"461593864589804",
      state:"{{csrf}}",
      version:"v1.1",
      fbAppEventsEnabled:true,
      debug:true,
      redirect: window.location.origin+'/me'
    }
  );
};

