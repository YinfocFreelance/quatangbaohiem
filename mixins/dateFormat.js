import { format } from 'date-fns'

export default {
  methods: {
    formatDate(datetime) {
      return format(datetime, 'DD-MM-YYYY')
    },
    formatDateYMD(datetime) {
      return format(datetime, 'YYYY-MM-DD')
    },

    formatDateTime(datetime) {
      return format(datetime, 'YYYY-MM-DD HH:mm:ss')
    },

    dateFormatDMY(datetime) {
      return format(datetime, 'DD-MM-YYYY')
    },
  }
}
