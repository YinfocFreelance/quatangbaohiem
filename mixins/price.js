import numeral from 'numeral'

export default {
  methods: {
    formatPrice(price) {
      if (!price && isNaN(price)) return ''
      return numeral(price).format('0,0[.]00')
    },

    reconvertPrice(price) {
      return parseInt(price.replace(/\,/g, ''))
    }
  }
}
