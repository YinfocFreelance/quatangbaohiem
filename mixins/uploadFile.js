import {objectToStringQuery, stringifyQuery} from '~/utils/route'
export default {
  methods: {
    async __uploadFile(file, type) {
      let payload = {
        Image: file,
        Type: type
      }

      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }
      const data = await this.$_callUserApiGet( 'image/upload', payload )
      if(data.Reason) {
        this.$toast.error(data.Reason,{duration: 5000})
        return false
      }
      else return data
    },

    async __isImage(fileName){
      if (!/\.(jpe?g|png|gif)$/i.test(fileName)) {
        return this.$toast.error('Only accept a image file')
      }
    },
    async __uploadFileMulti(url, files) {
      let f = new FormData()
      for( let i in files) {
        f.append('image[]', files[i])
      }
      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }

      return this.$axios.$post(url, f, config)
    },
  }
}
