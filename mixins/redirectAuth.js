export default {
  methods: {
    redirectOnAuthSuccess() {
      // Check has invite code
      const inviteCode = localStorage.getItem('invite_code')
      if (inviteCode) {
        return this.$router.push(this.$_toLocalePath(`/invite/${inviteCode}`))
      }
      let url = this.$route.query.return_url
      if (!url || url.match(/^\/\//)) url = '/'
      this.$router.push(url)
    },
  }
}
