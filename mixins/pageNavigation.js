import bPaginationNav from 'bootstrap-vue/es/components/pagination-nav/pagination-nav'
export default {
  components: {
    bPaginationNav,
  },

  methods: {
    linkGen(page) {
      if (this.$route.query.page) {
        return this.$route.fullPath.replace(`page=${this.$route.query.page}`, `page=${page}`)
      }
      if (this.$route.fullPath == this.$route.path) return `${this.$route.fullPath}?&page=${page}`
      return `${this.$route.fullPath}&page=${page}`
    },
  }
}
