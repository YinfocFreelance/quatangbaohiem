export default {
  methods: {
    $_injectScripts(option, parentSelector = 'head') {
      let script = document.createElement('script')
      Object.assign(script, option)
      console.log(script);
      return document.querySelector(parentSelector).appendChild(script)
    },
  }
}
