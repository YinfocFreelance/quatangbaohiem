import Cookie from 'js-cookie'
import {objectToStringQuery} from '~/utils/route'
export default {
  computed: {
    userInfo() {
      return Cookie.getJSON('user_info')
    }
  },

  methods: {
    async callApiGet(path, payload, pageSection = []) {
      if(pageSection) {
        payload.StartIndex = pageSection[0]
        payload.MaxResult = pageSection[1]
      }
      const data = await this.$axios.$get(path + objectToStringQuery(payload))
      if( data.Reason ) this.$toast.error(data.Reason, {duration: 5000})
      if( data.ErrorCode && data.ErrorCode === '001' ) {
        Cookie.remove('user_info')
        localStorage.removeItem('user_info')
      }
      return data
    },

    apiGetListQuery(payload, startIndex, perPage){
      let data = {}
      if(payload) data = payload
      data.StartIndex = startIndex
      data.MaxResult = perPage
      data.UserId = this.userInfo.userId
      data.Token = this.userInfo.token
      return objectToStringQuery(data)
    },

    apiGetQuery(payload) {
      let data = payload
      data.UserId = this.userInfo.userId
      data.Token = this.userInfo.token
      return objectToStringQuery(data)
    },
  }
}
