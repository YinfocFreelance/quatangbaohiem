import Vuex from 'vuex'

const store = () => {
  return new Vuex.Store({
    actions: {

    },
    modules: {
    }
  })
}

export default store
