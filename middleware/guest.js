
import { parseCookies } from '../utils/common'

export default function ({ req, redirect, store }) {
  if (process.client) {
    if (store.getters[AUTH_LOGGED_IN]) {
      redirect('/')
    }
  } else if (req.headers.cookie) {
    const cookie = parseCookies(req.headers.cookie)

    if (cookie.token) {
      redirect('/')
    }
  }
}
