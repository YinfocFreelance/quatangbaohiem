import Cookie from 'js-cookie'

const getRedirectUrl = url => '/#login'

export default function ({ req, redirect }) {
  if (process.client) {
    if (!Cookie.get('user_info')) {
      location.href = '/#login'
    }
  }
}
