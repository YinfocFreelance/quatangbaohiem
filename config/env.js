export default {
  API_END_POINT: 'http://visearch.net:8000/api',
  API_TIME_OUT: 30000, // ms
  APP_URL: 'https://luxstay.com',

  CLIENT_ID: '7e79e57e-2855-4ea9-bfd7-8bee31aa3235',

  FACEBOOK_APP_ID: '461593864589804',
  FB_MESSENGER_URL: 'http://m.me/luxstay',

  GOOGLE_CLIENT_ID: '173426758340-89jf9gg2jifift6fnm2o7n2q6g5hn5dh.apps.googleusercontent.com',

  HOTLINE: '18006586',
}
