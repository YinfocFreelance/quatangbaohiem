export default {
  API_END_POINT: 'https://api-sandbox.luxstay.com/api',
  API_TIME_OUT: 30000, // ms
  APP_URL: 'https://luxstay.com',

  CLIENT_ID: '7e79e57e-2855-4ea9-bfd7-8bee31aa3235',

  FACEBOOK_APP_ID: '536476913212966',
  FB_MESSENGER_URL: 'http://m.me/luxstay',

  GOOGLE_CLIENT_ID: '173426758340-89jf9gg2jifift6fnm2o7n2q6g5hn5dh.apps.googleusercontent.com',

  HERE_MAP_APP_ID: 'J0DIGVFVqmnsNZVXhohe',
  HERE_MAP_APP_CODE: '4s12N_M6CJ06n1smugaHCg',
  HOST_URL: 'https://host.luxstay.net',
  HOTLINE: '18006586',

  STRIPE_PUBLISHABLE_KEY: 'pk_test_u1Owgv36X1EZ9i6fVWQwSTG7',
  SENTRY_DSN: false,
  SENTRY_DISABLED: true,

  LST_BUSINESS_URL: 'https://business-dev.luxstay.net',
  LST_CRM_URL: 'https://crm.luxstay.com',

  //Tracking environment
  TRACKING_SETTING: {
    //Google Analytics
    ENABLE_GA: false,
    GA_CODE: 'UA-127243154-1',

    //Google Tag Manager
    ENABLE_GTM: false,
    GTM_CODE: 'GTM-PPXWZ74',

    //Facebook Pixel
    ENABLE_FACEBOOK_PIXEL: false,
    FACEBOOK_PIXEL_CODE: '752043235181068',

    //Criteo
    ENABLE_CRITEO: false,
    CRITEO_ACCOUNT_CODE: 54538,
    CRITEO_ACCOUNT_EMAIL: "nam.nguyenduy@luxstay.com",

    //Admicro
    ENABLE_ADMICRO: false,
    ADMICRO_ID: 2103,
    ADMICRO_DOMAIN: "luxstay.com",
    ADMICRO_TRACKING_ID: 5458,

    ENABLE_LST_CRM: false,

    ENABLE_ADWORD: false,
    ADWORD_1_ID: 'AW-756272930',
    ADWORD_1_CONVERSION_EVENT_ID: 'AW-756272930/BAzLCOSZg5cBEKKez-gC'
  }
}
